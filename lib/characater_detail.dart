import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:marvel_characters/model.dart';

class CharacterDetail extends StatefulWidget {
  final Results character;

  CharacterDetail({@required this.character});

  @override
  _CharacterDetailState createState() => _CharacterDetailState();
}

class _CharacterDetailState extends State<CharacterDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.character.name),
      ),
    );
  }
}
