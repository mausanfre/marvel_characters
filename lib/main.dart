import 'package:flutter/material.dart';
import 'package:marvel_characters/character_list_view.dart';

void main() {
  runApp(MaterialApp(
    title: "Marvel Characters App",
    home: CharacterListView(),
  ));
}
