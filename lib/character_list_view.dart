import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:marvel_characters/characater_detail.dart';
import 'package:marvel_characters/model.dart';
import 'package:http/http.dart' as http;

class CharacterListView extends StatefulWidget {
  @override
  _CharacterListViewState createState() => _CharacterListViewState();
}

class _CharacterListViewState extends State<CharacterListView> {
  List<Results> _characters = new List();
  int _offset = 0;
  int _limit = 10;
  int _totalItems = 0;

  Future<Null> _loadMoreData() async {
    var response = await http.get(
        'https://gateway.marvel.com/v1/public/characters?apikey=d89c919dbb61f12a1764b86bd8e8b73d&hash=da037efefed402e4a5f43a0a3a57c18e&ts=abcdefghijlmanopq&offset=$_offset&limit=$_limit');

    CharacterResponse characterResponse =
        CharacterResponse.fromJson(json.decode(response.body));

    if (characterResponse.code == 200 && characterResponse.data.count > 0) {
      _characters.addAll(characterResponse.data.results);
      setState(() {
        _totalItems = _characters.length;
        _offset = _totalItems;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Lista de personagens: ' + _totalItems.toString()),
        ),
        body: createListView());
  }

  Widget createListView() {
    return ListView.builder(
        itemCount: _characters.length + 1,
        itemBuilder: (context, index) {
          Results character =
              index < _characters.length ? _characters[index] : null;
          if (character != null) {
            return ListTile(
              trailing: Icon(Icons.arrow_right),
              leading: Image.network(
                '${character.thumbnail.path}.${character.thumbnail.extension}',
                height: 40,
                width: 40,
              ),
              title: Text(character.name),
              subtitle: Text(
                character.description,
                maxLines: 2,
                style: TextStyle(color: Colors.blue),
              ),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) =>
                          CharacterDetail(character: character),
                    ));
              },
            );
          } else {
            _loadMoreData();
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );
          }
        });
  }
}
